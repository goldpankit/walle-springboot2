package com.walle.core.constants;

/**
 * 框架级常量
 */
public interface Constants {

    // token请求头
    String HEADER_TOKEN = "walle-auth-token";

    // 2fa密码请求头
    String HEADER_2FA_PASSWORD = "X-2fa-Password";

    // 操作类型响应头
    String HEADER_OPERA_TYPE = "walle-opera-type";

    // 下载文件名称响应头
    String HEADER_DOWNLOAD_FILENAME = "walle-download-filename";

    /**
     * 缓存Key
     */
    interface CacheKey {

        // 系统配置，用于将系统配置存储在缓存中
        String SYSTEM_CONFIGS = "SYSTEM_CONFIGS";

        // 字典，用于将字典及数据存储在缓存中
        String DICTIONARIES = "DICTIONARIES";

        // 重复请求，用于防止重复调用时记录请求信息
        String REPEAT_REQUEST_PREFIX = "wall:prevent:repeat";

    }

    /**
     * 用户相关
     */
    interface User {

        // 绑定类型：手机号码
        String BIND_TYPE_PHONE_NUMBER = "PHONE_NUMBER";

        // 绑定类型：邮箱
        String BIND_TYPE_EMAIL = "EMAIL";
    }
}

package com.walle.api.user;

import com.walle.biz.user.UserLoginBiz;
import com.walle.core.model.ApiResponse;
import com.walle.core.model.LoginUserInfo;
import com.walle.core.prevent.PreventRepeat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@Api(tags = "用户登录")
@RestController
@RequestMapping("/login")
public class UserLoginController {

    @Resource
    private UserLoginBiz userLoginBiz;

    @ApiOperation("模拟登录")
    @PostMapping("/mock")
    public ApiResponse<LoginUserInfo> loginByMock (HttpServletResponse response) {
        return ApiResponse.success(userLoginBiz.loginByMock(response));
    }

}

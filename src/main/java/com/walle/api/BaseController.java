package com.walle.api;

import com.walle.core.constants.Constants;
import com.walle.core.constants.ResponseStatus;
import com.walle.core.exception.BusinessException;
import com.walle.core.model.LoginUserInfo;
import com.walle.core.utils.Utils;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class BaseController {

    /**
     * 获取当前登录用户
     */
    protected LoginUserInfo getLoginUser (HttpServletRequest request) {
        LoginUserInfo loginUserInfo =  Utils.Session.getLoginUserInfo(request);
        if (loginUserInfo == null) {
            throw new BusinessException(ResponseStatus.UN_LOGIN);
        }
        return loginUserInfo;
    }

    /**
     * 获取登录用户ID（允许为null）
     *
     * @param request HttpServletRequest
     * @return 用户ID
     */
    protected Integer getLoginUserIdAllowNull (HttpServletRequest request) {
        LoginUserInfo loginUserInfo =  Utils.Session.getLoginUserInfo(request);
        if (loginUserInfo == null) {
            return null;
        }
        return loginUserInfo.getId();
    }

    /**
     * 获取登录用户ID
     *
     * @param request HttpServletRequest
     * @return 用户ID
     */
    protected Integer getLoginUserId (HttpServletRequest request) {
        return this.getLoginUser(request).getId();
    }

    /**
     * 获取ID集合
     *
     * @param ids 使用","隔开的多个ID
     * @return List<Integer>
     */
    protected List<Integer> getIdList (String ids) {
        String [] idArray = ids.split(",");
        List<Integer> idList = new ArrayList<>();
        for (String id : idArray) {
            idList.add(Integer.valueOf(id));
        }
        return idList;
    }

    /**
     * 获取文件字节流
     *
     * @param is 输入流
     * @return ByteArrayOutputStream
     */
    protected ByteArrayOutputStream getByteArrayOutputStream(InputStream is) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] bs = new byte[is.available()];
        int len;
        while ((len = is.read(bs)) != -1) {
            os.write(bs, 0, len);
        }
        return os;
    }

    /**
     * 下载字节流
     *
     * @param filename 文件名称
     * @param baos 字节流
     * @param response 响应对象
     */
    protected void downloadByteArray (String filename, ByteArrayOutputStream baos, HttpServletResponse response) throws IOException{
        String encodeFileName = URLEncoder.encode(filename, StandardCharsets.UTF_8.toString());
        response.setHeader("Content-Disposition","attachment;filename=" + encodeFileName);
        response.setContentType("application/octet-stream");
        response.setHeader(Constants.HEADER_OPERA_TYPE, "download");
        response.setHeader(Constants.HEADER_DOWNLOAD_FILENAME, encodeFileName);
        response.getOutputStream().write(baos.toByteArray());
    }
}

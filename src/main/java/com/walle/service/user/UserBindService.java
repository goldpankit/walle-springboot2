package com.walle.service.user;

import com.walle.dao.user.UserBindMapper;
import com.walle.dao.user.model.UserBind;
import com.walle.service.BaseService;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class UserBindService extends BaseService<UserBind, UserBindMapper> {

    public UserBindService(UserBindMapper mapper, Environment environment) {
        super(mapper, environment);
    }
}

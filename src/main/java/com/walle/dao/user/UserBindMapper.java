package com.walle.dao.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.walle.dao.user.model.UserBind;
import org.springframework.stereotype.Component;

@Component
public interface UserBindMapper extends BaseMapper<UserBind> {
}

package com.walle.dao.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.walle.dao.user.model.User;
import org.springframework.stereotype.Component;

@Component
public interface UserMapper extends BaseMapper<User> {
}

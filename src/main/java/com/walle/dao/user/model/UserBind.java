package com.walle.dao.user.model;

import com.walle.dao.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@Accessors(chain = true)
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel("用户信息绑定")
@TableName("`user_bind`")
public class UserBind extends BaseModel {

    @TableField("`user_id`")
    @ApiModelProperty(value="用户ID")
    private Integer userId;

    @TableField("`bind_type`")
    @ApiModelProperty(value="绑定类型")
    private String bindType;

    @TableField("`bind_value_digest`")
    @ApiModelProperty(value="绑定值摘要")
    private String bindValueDigest;

    @TableField("`bind_value_cipher`")
    @ApiModelProperty(value="绑定值密文")
    private String bindValueCipher;

}

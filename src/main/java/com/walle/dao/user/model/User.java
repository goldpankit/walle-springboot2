package com.walle.dao.user.model;

import com.walle.dao.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@Accessors(chain = true)
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel("用户")
@TableName("`user`")
public class User extends BaseModel {

    @TableField("`username`")
    @ApiModelProperty(value="用户名")
    private String username;

    @TableField("`nickname`")
    @ApiModelProperty(value="昵称")
    private String nickname;

    @TableField("`avatar`")
    @ApiModelProperty(value="头像")
    private String avatar;

    @TableField("`from`")
    @ApiModelProperty(value="来源")
    private String from;

    @TableField("`remark`")
    @ApiModelProperty(value="备注")
    private String remark;

}

## 基础信息

Wall-E旨在研发一套稳定、易用C端服务基础工程，可与其他Wall-E系的前端基础工程自由搭配来完成一整个项目的研发。后台管理系统可使用Eva系基础工程来完成，两者搭配效果更佳！

## 项目特点
1. 可扩展的功能模块，默认情况下提供了用户模块的模拟登录接口，使用GoldPanKit可进一步进行源码级功能模块的扩展，如发送短信、微信小程序登录、短信登录等。
2. 不用担心存在BUG，如果存在BUG，使用GoldPanKit可实现一键升级。
3. 不用担心存在安全漏洞，如果存在安全漏洞，GoldPanKit会进行提醒并支持一键升级。
4. 清华团队全职维护，无版权，可放心使用。
5. 规范化代码 + 详细的代码注释。
6. 丰富的插件市场，可使用GoldPanKit进行单表、多表的接口生成。